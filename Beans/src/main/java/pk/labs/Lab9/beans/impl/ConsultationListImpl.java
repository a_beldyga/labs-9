package pk.labs.Lab9.beans.impl;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import pk.labs.Lab9.beans.Consultation;
import pk.labs.Lab9.beans.ConsultationList;

public class ConsultationListImpl implements ConsultationList, PropertyChangeListener{
    
    private List<Consultation> konsultacje;
    private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);

    
    public ConsultationListImpl() {
        konsultacje = new ArrayList<Consultation>();
        pcs.addPropertyChangeListener(this);
    }
  
    
    @Override
    public int getSize(){
        return konsultacje.size();
    }

    @Override
    public Consultation[] getConsultation(){
        Consultation[] c = {};
        return konsultacje.toArray(c);
    }
    
    public void setConsultation(Consultation[] konsultacje)
    {
        this.konsultacje.clear();
        this.konsultacje.addAll(Arrays.asList(konsultacje));
        
    
    }
    
    @Override
    public Consultation getConsultation(int index){
        return konsultacje.get(index);
    }
    
    @Override
    public void addConsultation(Consultation consultation) throws PropertyVetoException{
        int size1 = getSize();
        if(consultation != null){
            konsultacje.add(consultation);
            pcs.firePropertyChange("consultation", size1, getSize());
        }
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener){
        pcs.removePropertyChangeListener(listener);
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener){
        pcs.addPropertyChangeListener(listener);
    }
    
    public void propertyChange(PropertyChangeEvent evt) {
        
    }
    
}