/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.labs.Lab9.beans.impl;

import java.beans.PropertyVetoException;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import pk.labs.Lab9.beans.Consultation;
import pk.labs.Lab9.beans.Term;

@XmlRootElement
public class ConsultationImpl implements Consultation, 
        Serializable{

    protected String student;

    protected Date beginDate;
    protected Date endDate;

    public ConsultationImpl() {
    }
    
    @Override
    public String getStudent() {
        return student;
    }
    
    @Override
    public void setStudent(String student) {
        this.student = student;
    }

    @Override
    public Date getBeginDate() {
        return beginDate;
    }
    
    public void setBeginDate(Date data)
    {
    this.beginDate = data;
    }

    @Override
    public Date getEndDate() {
        return endDate;
    }
    
    public void setEndDate(Date data)
    {
        this.endDate = data;
    }

    @Override
    @XmlElement
    public void setTerm(Term term) throws PropertyVetoException {
        beginDate = term.getBegin();
        endDate = term.getEnd();
    }

    public Term getTerm ()
    {
    return this.getTerm();
    }
    
    @Override
    public void prolong(int minutes) throws PropertyVetoException {
        Calendar cal = Calendar.getInstance();
        cal.setTime(endDate);
        if (minutes > 0) {
            cal.add(Calendar.MINUTE, minutes);
        }
        else 
        {
            cal.add(Calendar.MINUTE, 0);
        }
        endDate = cal.getTime();
    }
}
