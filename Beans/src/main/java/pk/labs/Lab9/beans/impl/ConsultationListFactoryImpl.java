/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.Lab9.beans.impl;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import pk.labs.Lab9.beans.ConsultationList;
import pk.labs.Lab9.beans.ConsultationListFactory;

/**
 *
 * @author maciej
 */
public class ConsultationListFactoryImpl implements ConsultationListFactory, Serializable {

    @Override
    public ConsultationList create() {
              
        return new ConsultationListImpl();
    }

    @Override
    public ConsultationList create(boolean deserialize)
    {
        ConsultationList lista = new ConsultationListImpl();
        try {
            XMLDecoder decoder = new XMLDecoder(new BufferedInputStream(new  FileInputStream("test.xml")));
            
             lista = (ConsultationListImpl)decoder.readObject();
            decoder.close();
           
      
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ConsultationListFactoryImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista; 
    }

    @Override
    public void save(ConsultationList consultationList
    ) {
        try {
            XMLEncoder encoder = new XMLEncoder(new BufferedOutputStream(new FileOutputStream("test.xml")));
            encoder.writeObject(consultationList);
            encoder.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ConsultationListFactoryImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
//        FileOutputStream fOut;
//        try {
//            fOut = new FileOutputStream("test.xml");
//            ObjectOutput out = new ObjectOutputStream(fOut);
//            out.writeObject(consultationList);
//             out.flush();
//           out.close();
//        } catch (FileNotFoundException ex) {
//            Logger.getLogger(ConsultationListFactoryImpl.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        catch (IOException ex)
//        {
//        }
//        }

}
