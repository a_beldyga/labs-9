package pk.labs.Lab9.beans.impl;

import java.util.Calendar;
import java.util.Date;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import pk.labs.Lab9.beans.Term;

@XmlRootElement
public class TermImpl implements Term{

    protected Date begin;
    protected int duration;
    
    @Override
    public Date getBegin() {
        return begin;
    }

    @Override
    @XmlElement
    public void setBegin(Date begin) {
        this.begin = begin;
    }

    @Override
    public int getDuration() {
        return duration;
    }

    @Override
    @XmlElement
    public void setDuration(int duration) {
        if (duration > 0) {
            this.duration = duration; 
        }
    }

    @Override
    public Date getEnd() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(begin);
        
        if (duration > 0) {
            cal.add(Calendar.MINUTE, duration);
        }
        else
        {
            cal.add(Calendar.MINUTE, 30);
        }
        
        return cal.getTime();
    }
    
}
